# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- update version requirement of _capistrano-bundler_ in gemspec

## [0.2.1] - 2017-04-08
### Fixed
- removed dependency of _capistrano-git-copy_ from gemspec

## [0.2.0] - 2017-04-08
### Changed
- removed dependency of _capistrano-git-copy_

## [0.1.0] - 2015-08-26
### Changed
- package gems
- check for changes to prevent uploading of all gems on every deploy
- allow caching before actual deploy with standalone task
- task to clear local and remote cache
