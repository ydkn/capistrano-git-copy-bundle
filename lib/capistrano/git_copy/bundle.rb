# frozen_string_literal: true

require 'capistrano/bundler'
require 'capistrano/git_copy/bundle/utility'
require 'capistrano/git_copy/bundle/task'
